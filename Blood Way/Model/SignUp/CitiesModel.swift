//
//  CitiesModel.swift
//  Blood Way
//
//  Created by Fahim Rahman on 3/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import Foundation

struct citiesModel : Codable {
    let id : Int?
    let locationName : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case locationName = "locationName"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        locationName = try values.decodeIfPresent(String.self, forKey: .locationName)
    }

}
