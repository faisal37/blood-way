//
//  PasswordChangeResponseModel.swift
//  Blood Way
//
//  Created by Mac Mini on 16/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import Foundation

struct PasswordChangeResponseModel : Codable {
    
    let detail : String?
    
   

//    enum CodingKeys: String, CodingKey {
//
//        case old_password = "old_password"
//        case detail = "detail"
//    }
//
//
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        let detail = try values.decodeIfPresent(String.self, forKey: .detail)
//    }
    
}



struct pwd: Decodable {

    var err: [String]?
    var success = String()

    enum CodingKeys: String, CodingKey {
        case detail = "detail"
        case old_password = "old_password"
        case new_password2 = "new_password2"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        do {
            success = try container.decode(String.self, forKey: .detail)
        } catch {
            do {
                err = try container.decode([String].self, forKey: .old_password)
            } catch {
                err = try container.decode([String].self, forKey: .new_password2)
            }
        }
    }
}

struct pwdd : Codable {
    let old_password : [String]?

    enum CodingKeys: String, CodingKey {

        case old_password = "old_password"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        old_password = try values.decodeIfPresent([String].self, forKey: .old_password)
    }

}
