import Foundation
struct ProfileModel : Codable {
	
    let pk : Int?
	let email : String?
	let username : String?
	let area : String?
	let city : String?
	let blood_group : String?
	let mobile_number : String?
	let user_image : String?
    let gender: String?
    let weight: String?
	let receverinfo : [Receverinfo]?
	//let status : Bool?
    //let age : String?

	enum CodingKeys: String, CodingKey {

		case pk = "pk"
		case email = "email"
		case username = "username"
		case area = "area"
		case city = "city"
		case blood_group = "blood_group"
		case gender = "gender"
        case weight = "weight"
		case mobile_number = "mobile_number"
		case user_image = "user_image"
		case receverinfo = "receverinfo"
		//case status = "status"
        //case age = "age"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		pk = try values.decodeIfPresent(Int.self, forKey: .pk)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		area = try values.decodeIfPresent(String.self, forKey: .area)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		blood_group = try values.decodeIfPresent(String.self, forKey: .blood_group)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        weight = try values.decodeIfPresent(String.self, forKey: .weight)
		mobile_number = try values.decodeIfPresent(String.self, forKey: .mobile_number)
		user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
        receverinfo = try values.decodeIfPresent([Receverinfo].self, forKey: .receverinfo)
		//status = try values.decodeIfPresent(Bool.self, forKey: .status)
        //age = try values.decodeIfPresent(String.self, forKey: .age)
	}
}
