//
//  RecieverInfoModel.swift
//  Blood Way
//
//  Created by Fahim Rahman on 16/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

struct Receverinfo : Codable {

    let pk : Int?
    let message : String?
    let donationLocation : String?
    let status : Bool?
    let receverid : Int?
    let senderid : Int?
    let senderNumber : String?
    let senderLocation : String?

    enum CodingKeys: String, CodingKey {

        case pk = "pk"
        case message = "message"
        case donationLocation = "donationLocation"
        case status = "status"
        case receverid = "receverid"
        case senderid = "senderid"
        case senderNumber = "senderNumber"
        case senderLocation = "senderLocation"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pk = try values.decodeIfPresent(Int.self, forKey: .pk)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        donationLocation = try values.decodeIfPresent(String.self, forKey: .donationLocation)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        receverid = try values.decodeIfPresent(Int.self, forKey: .receverid)
        senderid = try values.decodeIfPresent(Int.self, forKey: .senderid)
        senderNumber = try values.decodeIfPresent(String.self, forKey: .senderNumber)
        senderLocation = try values.decodeIfPresent(String.self, forKey: .senderLocation)
    }
}

