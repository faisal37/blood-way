import UIKit
struct ProfileModel: Codable {

  enum CodingKeys: String, CodingKey {
    case age
    case username
    case bloodGroup = "blood_group"
    case email
    case weight
    case gender
    case status
    case pk
    case area
    case receverinfo
    case mobileNumber = "mobile_number"
    case city
  }

  var age: String?
  var username: String?
  var bloodGroup: String?
  var email: String?
  var weight: String?
  var gender: String?
  var status: Bool?
  var pk: Int?
  var area: String?
  var receverinfo: [Receverinfo]?
  var mobileNumber: String?
  var city: Int?



  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    age = try container.decodeIfPresent(String.self, forKey: .age)
    username = try container.decodeIfPresent(String.self, forKey: .username)
    bloodGroup = try container.decodeIfPresent(String.self, forKey: .bloodGroup)
    email = try container.decodeIfPresent(String.self, forKey: .email)
    weight = try container.decodeIfPresent(String.self, forKey: .weight)
    gender = try container.decodeIfPresent(String.self, forKey: .gender)
    status = try container.decodeIfPresent(Bool.self, forKey: .status)
    pk = try container.decodeIfPresent(Int.self, forKey: .pk)
    area = try container.decodeIfPresent(String.self, forKey: .area)
    receverinfo = try container.decodeIfPresent([Receverinfo].self, forKey: .receverinfo)
    mobileNumber = try container.decodeIfPresent(String.self, forKey: .mobileNumber)
    city = try container.decodeIfPresent(Int.self, forKey: .city)
  }

}

struct ProfileModel2 : Codable {
    let pk : Int?
    let email : String?
    let username : String?
    let area : String?
    let city : Int?
    let blood_group : String?
    let age : String?
    let mobile_number : String?
    let user_image : String?
    let status : Bool?
    let gender : String?
    let weight : String?
    let receverinfo : [Receverinfo]?

    enum CodingKeys: String, CodingKey {

        case pk = "pk"
        case email = "email"
        case username = "username"
        case area = "area"
        case city = "city"
        case blood_group = "blood_group"
        case age = "age"
        case mobile_number = "mobile_number"
        case user_image = "user_image"
        case status = "status"
        case gender = "gender"
        case weight = "weight"
        case receverinfo = "receverinfo"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        pk = try values.decodeIfPresent(Int.self, forKey: .pk)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        area = try values.decodeIfPresent(String.self, forKey: .area)
        city = try values.decodeIfPresent(Int.self, forKey: .city)
        blood_group = try values.decodeIfPresent(String.self, forKey: .blood_group)
        age = try values.decodeIfPresent(String.self, forKey: .age)
        mobile_number = try values.decodeIfPresent(String.self, forKey: .mobile_number)
        user_image = try values.decodeIfPresent(String.self, forKey: .user_image)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        gender = try values.decodeIfPresent(String.self, forKey: .gender)
        weight = try values.decodeIfPresent(String.self, forKey: .weight)
        receverinfo = try values.decodeIfPresent([Receverinfo].self, forKey: .receverinfo)
    }

}
