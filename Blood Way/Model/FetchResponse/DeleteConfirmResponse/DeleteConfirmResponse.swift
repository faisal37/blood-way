//
//  DeleteConfirmResponse.swift
//  Blood Way
//
//  Created by S. M. Akib Al Faisal on 1/4/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import Foundation

struct DeleteResponseConfirm:Codable{
    let response:String?
}
