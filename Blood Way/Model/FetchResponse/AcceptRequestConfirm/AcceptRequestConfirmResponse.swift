//
//  AcceptRequestConfirmResponse.swift
//  Blood Way
//
//  Created by S. M. Akib Al Faisal on 31/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import Foundation
struct AcceptRequestConfirm : Codable {
    let response : String?
    let pk : Int?
    let status : Bool?

    enum CodingKeys: String, CodingKey {

        case response = "response"
        case pk = "pk"
        case status = "status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        response = try values.decodeIfPresent(String.self, forKey: .response)
        pk = try values.decodeIfPresent(Int.self, forKey: .pk)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
    }

}
