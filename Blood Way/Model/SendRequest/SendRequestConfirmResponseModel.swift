//
//  SendRequestConfirmResponse.swift
//  Blood Way
//
//  Created by Fahim Rahman on 10/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

struct SendReqConfirmModel : Codable {
    
    let response : String?
    let pk : Int?
    let message : String?
    let donationLocation : String?
    let status : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case response = "response"
        case pk = "pk"
        case message = "message"
        case donationLocation = "donationLocation"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        response = try values.decodeIfPresent(String.self, forKey: .response)
        pk = try values.decodeIfPresent(Int.self, forKey: .pk)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        donationLocation = try values.decodeIfPresent(String.self, forKey: .donationLocation)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
    }
    
}
