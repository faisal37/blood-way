//
//  requestForBloodModel.swift
//  Blood Way
//
//  Created by Fahim Rahman on 15/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import Foundation
import SwiftyJSON

struct RequestForBlood {

    let pk: String?
    let username: String?
    let area: String?
    let city: String?
    let bloodGroup: String?
    let mobileNumber: String?
    let userImage: Any?

    
    init(_ json: JSON) {
        
        pk = json["pk"].stringValue
        username = json["username"].stringValue
        area = json["area"].stringValue
        city = json["city"].stringValue
        bloodGroup = json["blood_group"].stringValue
        mobileNumber = json["mobile_number"].stringValue
        userImage = json["user_image"]
    }
}
