//
//  AcceptedUserResponseModel.swift
//  Blood Way
//
//  Created by S. M. Akib Al Faisal on 29/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import Foundation

struct AcceptedUserModel : Codable {
        let pk : Int?
        let receverName : String?
        let receverBloodGroup : String?
        let status : Bool?
        let receverid : Int?
        let senderid : Int?
        let receverNumber : String?
        let receverLocation : String?

        enum CodingKeys: String, CodingKey {

            case pk = "pk"
            case receverName = "receverName"
            case receverBloodGroup = "receverBloodGroup"
            case status = "status"
            case receverid = "receverid"
            case senderid = "senderid"
            case receverNumber = "receverNumber"
            case receverLocation = "receverLocation"
        }

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            pk = try values.decodeIfPresent(Int.self, forKey: .pk)
            receverName = try values.decodeIfPresent(String.self, forKey: .receverName)
            receverBloodGroup = try values.decodeIfPresent(String.self, forKey: .receverBloodGroup)
            status = try values.decodeIfPresent(Bool.self, forKey: .status)
            receverid = try values.decodeIfPresent(Int.self, forKey: .receverid)
            senderid = try values.decodeIfPresent(Int.self, forKey: .senderid)
            receverNumber = try values.decodeIfPresent(String.self, forKey: .receverNumber)
            receverLocation = try values.decodeIfPresent(String.self, forKey: .receverLocation)
        }

}
