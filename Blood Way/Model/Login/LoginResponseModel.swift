//
//  Login.swift
//  Blood Way
//
//  Created by Fahim Rahman on 6/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import Foundation

    struct LoginResponseModel : Codable {
    let response : String?
    let pk : Int?
    let email : String?
    let token : String?

}
