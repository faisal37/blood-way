//
//  LoginRequestModel.swift
//  Blood Way
//
//  Created by Fahim Rahman on 7/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import Foundation

struct LoginRequestModel: Codable {

    let username: String?
    let password: String?

}
