//
//  LoginViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 6/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

var token: String?
var pk: String?

class LoginViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
    
    //var responseModel = [LoginResponseModel]()
    
    //Outlets
    
    @IBOutlet weak var phoneNumberTextField: UnderlinedTextField!
    
    @IBOutlet weak var passwordTextField: UnderlinedTextField!
    
    @IBOutlet weak var login: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationItem.setHidesBackButton(true, animated: true);
        
        //navigationController?.navigationBar.backItem?.title = ""
        self.title = "Login"
        
        self.phoneNumberTextField.becomeFirstResponder()

        login.layer.cornerRadius = 25
        phoneNumberTextField.text = ""
        passwordTextField.text = ""
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = ""
        self.title = "Login"
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
        if(UserDefaults.standard.getValueofLogin()==true){
            let vc: MenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else{
            
        }
    }
    
    // Login Button
    @IBAction func loginButton(_ sender: UIButton) {
        
        if phoneNumberTextField.text != "" && passwordTextField.text != "" {
        loginMethod()
        }
        else {
            alertUser(alertTitle: "ERROR!", alertMessage: "Enter Password Correctly", actionTitle: "OK")
        }
    }
    
    // Forget button
    @IBAction func forgotPasswordButton(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let vc = storyboard.instantiateViewController(withIdentifier: "ResetPasswordViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    //Post login credentials to the server
    
    func loginMethod() {
        
        guard let url = URL(string: "\(baseUrl)api/v1/account/login") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let parameters: [String: Any] = [
            "username" : "\(phoneNumberTextField.text!)",
            "password" : "\(passwordTextField.text!)"
        ]
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let data = data {
                do {
                    let responseModel = try JSONDecoder().decode(LoginResponseModel.self, from: data)
                    
                    DispatchQueue.main.async {
                        
                        token = responseModel.token
                        pk = "\(responseModel.pk ?? 0)"
                        
                        
                        
                        
        
                        if responseModel.response! == "successfullylogin" {
                            
                            self.setToken(token: token ?? "", pk: pk ?? "") //Save Token & Pk to keychain
                            
                            print("token : "+self.getToken(value: "Token"))
                            print("pk : "+self.getToken(value: "PK"))
                           
                            UserDefaults.standard.setValueForLogin(value: true)
                            
                            let vc: MenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else {
                            
                            self.alertUser(alertTitle: "ERROR!", alertMessage: "Your email or password does't match or you do not have an account. Please sign up", actionTitle: "OK")
                            //self.phoneNumberTextField.text = ""
                            self.passwordTextField.text = ""
                        }
                    }
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        }.resume()
    }
    
    
    func alertUser(alertTitle: String, alertMessage: String, actionTitle: String) {
           let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: nil ))
           self.present(alert, animated: true, completion: nil)
       }
    
    
}


