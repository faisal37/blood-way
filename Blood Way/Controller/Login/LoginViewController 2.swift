//
//  LoginViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 6/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

var token: String?
var pk: String?

class LoginViewController: UIViewController {
    
    var responseModel = [LoginResponseModel]()
    
    //Outlets
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var login: UIButton!
    
    var i:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        login.layer.cornerRadius = 25
        phoneNumberTextField.text = ""
        passwordTextField.text = ""
        i = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("inside here!")
        phoneNumberTextField.text = ""
        passwordTextField.text = ""
    }
    
    
    // Login Button
    @IBAction func loginButton(_ sender: UIButton) {
        loginMethod()
        
    }
    
    // Forget button
    @IBAction func forgotPasswordButton(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "ResetPassword", bundle: nil)
        
         let vc: ResetPasswordViewController = storyboard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
                            self.navigationController?.pushViewController(vc, animated: true)    }
    
    
    // Post login credentials to the server
    func loginMethod() {
         
         let loginParameter = LoginRequestModel(username: "\(phoneNumberTextField.text!)", password: "\(passwordTextField.text!)")
         
         let headers: HTTPHeaders = ["Accept": "application/json"]
         
         AF.request("\(baseUrl)api/v1/account/login",
             method: .post,
             parameters: loginParameter, headers: headers
         ).response { response in
    
             let items = JSON(response.value as Any)
             
            // DispatchQueue.main.async {
                 
                 let model = LoginResponseModel(items)
                 token = model.token!
                 pk = model.pk!
                 
                 if model.response! == "successfullylogin" {
                     
                     let vc: MenuViewController = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
                     self.navigationController?.pushViewController(vc, animated: true)
                 }
                 else {
                     print("Your email or password does't match or you do not have an account. Please sign up")
                     self.phoneNumberTextField.text = ""
                     self.passwordTextField.text = ""
                 }
             //}
         } .resume()
     }
}
