//
//  ResetPasswordViewController.swift
//  Blood Way
//
//  Created by Mac Mini on 6/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//
struct ResetPassword:Codable{
    let detail:String?
}

import UIKit

class ResetPasswordViewController: UIViewController {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var resetButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailTextField.becomeFirstResponder()
        emailTextField.text = ""
        
        resetButton.layer.cornerRadius = 25
    }
    
    @IBAction func resetPasswordAction(_ sender: Any) {
         if emailTextField.text != "" {
            sendPasswordResetRequest()
        }else{
            alertUser(alertTitle: "ERROR!", alertMessage: "Email Field Can not be blank.", actionTitle: "OK")
        }
    }
    
  
    
    func sendPasswordResetRequest(){
        guard let url = URL(string: "\(baseUrl)api/v1/rest-auth/password/reset/") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let parameters: [String: Any] = ["email" : "\(emailTextField.text!)"]
        //["email" : "faisal@gmail.com"]
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let data = data {
                do {
                    let responseModel = try JSONDecoder().decode(ResetPassword.self, from: data)
                    //print(responseModel)
                    
                    DispatchQueue.main.async {
                        self.alertUser(alertTitle: "Confirm", alertMessage: responseModel.detail ?? "", actionTitle: "OK")
                    }
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        }.resume()
    }
    

         //###########################################################
        func alertUser(alertTitle: String, alertMessage: String, actionTitle: String) {
            let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    
}

