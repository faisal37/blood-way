//
//  AuthViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 6/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

let baseUrl = "http://103.192.157.61:88/"
//let baseUrl = "http://192.168.220.44:8000/"
//let baseUrl = "http://127.0.0.1:8000/"


class AuthViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
    
    @IBOutlet weak var authPageLogin: UIButton!
    
    @IBOutlet weak var authPageSignUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated: true);
        self.title = "Log In or Sign Up"
        
        authPageLogin.layer.cornerRadius = 25
        authPageSignUp.layer.cornerRadius = 25
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.title = "Log In or Sign Up"
    }
    
    
    @IBAction func loginButton(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
        navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func signUpButton(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignUpViewController")
        navigationController?.pushViewController(vc!, animated: true)
        
    }
}

extension UINavigationController {
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
