//
//  TableViewCell.swift
//  Blood Way
//
//  Created by Fahim Rahman on 16/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {

    @IBOutlet weak var profileData: UILabel!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var indicatorImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
