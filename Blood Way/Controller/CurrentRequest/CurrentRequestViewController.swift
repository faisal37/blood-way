import UIKit

class CurrentRequestViewController: UIViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    var responsedata = [Receverinfo]()
    var recieverInfoProfiles = [String]()
    
    var currentTableView: Int?
    
    @IBOutlet weak var currentRequestTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Current Request"
        
        currentRequestTableView.dataSource = self
        currentRequestTableView.delegate = self
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Clear List", style: .plain, target: self, action: #selector(clearCurrentRequestList))
        
        dataloadfromServer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Current Request"
    }
    
    
    @objc func clearCurrentRequestList(){
        print("clearCurrentRequestList Action")
    }
    
    func dataloadfromServer() {
        
        guard let url = URL(string: "\(baseUrl)api/v1/account/\(pk!)") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        //print(token!)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    let responseModel = try JSONDecoder().decode(ProfileModel.self, from: data)
                    
                    let receiverInfo = responseModel.receverinfo
                    
                    print("current request test")
                    print(responseModel)
                    
                    self.responsedata = receiverInfo ?? []
                    
                    print(self.responsedata)
                    
                    DispatchQueue.main.async {
                        self.currentRequestTableView.reloadData()
                    }
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        } .resume()
    }
}




extension CurrentRequestViewController:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responsedata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CurrentRequestViewCell = tableView.dequeueReusableCell(withIdentifier: "currentRequestCell", for: indexPath) as! CurrentRequestViewCell
        
        
//        recieverInfoProfiles.append("\(responsedata[indexPath.row].pk!)")
//            print(recieverInfoProfiles)
        // ..... // .....  // ....... // have to load profile name using PK
        
        
        
        
        cell.userName.text = responsedata[indexPath.row].senderName?.capitalizingFirstLetter()
        cell.contactNumber.text = responsedata[indexPath.row].senderNumber
        cell.userLocation.text = responsedata[indexPath.row].senderLocation
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Number of cell Clicked",indexPath.row)
        
        print(responsedata[indexPath.row].pk ?? 99)
        
        let storyboard = UIStoryboard(name: "CurrentRequest", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AcceptBloodRequestViewController") as? AcceptBloodRequestViewController
        vc?.acceptPk = "\(responsedata[indexPath.row].senderid!)"
        vc?.notificationID = "\(responsedata[indexPath.row].pk!)"
        
        navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
    }
}
