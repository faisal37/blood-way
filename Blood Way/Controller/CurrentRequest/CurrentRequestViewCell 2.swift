import UIKit

class CurrentRequestViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var contactNumber: UILabel!
    
    @IBOutlet weak var userLocation: UILabel!
    
    @IBOutlet weak var message: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    public func configuredatafunction(with json:Receverinfo){
//
//        //profileImage.image = json.
//        //userName.text = json.
//        contactNumber.text = json.senderNumber ?? ""
//        message.text = json.message ?? ""
//        userLocation.text = json.donationLocation ?? ""
//
//    }

}
