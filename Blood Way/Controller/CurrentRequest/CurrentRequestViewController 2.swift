import UIKit

class CurrentRequestViewController: UIViewController {
    
    var responsedata = [Receverinfo]()
    var recieverInfoProfiles = [String]()
    
    var currentTableView: Int?
    
    @IBOutlet weak var currentRequestTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentRequestTableView.dataSource = self
        currentRequestTableView.delegate = self
        
        dataloadfromServer()
    }
    
    func dataloadfromServer() {
        
        guard let url = URL(string: "\(baseUrl)api/v1/account/\(pk!)") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token 685dc509e52d34adbb8df473d8ab959a35fd7ba5", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    let responseModel = try JSONDecoder().decode(ProfileModel.self, from: data)
                    
                    let receiverInfo = responseModel.receverinfo
                    
                    DispatchQueue.main.async {
                        
                        //print(receiverInfo!)
                        
                        self.responsedata = receiverInfo!
                        self.currentRequestTableView.reloadData()
                    }
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        } .resume()
    }
    
    @IBAction func segment(_ sender: UISegmentedControl) {
        
        currentTableView = sender.selectedSegmentIndex
        
        if currentTableView == 0 {
            
        }
        else {
            
        }
        
    }
    
    
}




extension CurrentRequestViewController:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responsedata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CurrentRequestViewCell = tableView.dequeueReusableCell(withIdentifier: "currentRequestCell", for: indexPath) as! CurrentRequestViewCell
        
        
        //recieverInfoProfiles.append("\(responsedata[indexPath.row].pk!)")
        //print(recieverInfoProfiles)
        // ..... // .....  // ....... // have to load profile name using PK
        
        
        cell.contactNumber.text = responsedata[indexPath.row].senderNumber
        cell.message.text = responsedata[indexPath.row].message
        cell.userLocation.text = responsedata[indexPath.row].donationLocation
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Number of cell Clicked",indexPath.row)
        
        let storyboard = UIStoryboard(name: "CurrentRequest", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AcceptBloodRequest") as? AcceptBloodRequestViewController
        vc?.acceptPk = "\(responsedata[indexPath.row].pk!)"
        navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(view.frame.height/5)
    }
}
