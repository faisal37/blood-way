import UIKit

class CurrentRequestViewCell: UITableViewCell {
    
    
    @IBOutlet weak var currentRequestView: UIView!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var contactNumber: UILabel!
    
    @IBOutlet weak var userLocation: UILabel!
    
    @IBOutlet weak var message: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        currentRequestView.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  

}
