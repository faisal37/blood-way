import UIKit

class AcceptBloodRequestViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var contactNumber: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var bloodGroup: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var donationComplete: UIButton!
    
    var acceptPk = String()
    var notificationID = String()
    
    struct DeleteResponse:Codable{
        let response:String?
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = "Current Request"
        self.title = "Requested User"
        
        acceptButton.layer.cornerRadius = 25
        donationComplete.layer.cornerRadius = 25
        
        //removeSaveValue()
        
        print("comp: \(getSaveValue(value: "completeButton"))")
        
       setButtonState()
        
        
        getRequesterData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.backItem?.title = "Current Request"
        self.title = "Requested User"
    }
    
    
    func setButtonState(){
        
         if(getSaveValue(value: "completeButton") == ""){
                   donationComplete.isHidden = true
            }
         if(getSaveValue(value: "completeButton") == "true"){
                   donationComplete.isHidden = false
            }
         if(getSaveValue(value: "completeButton") == "false"){
                   donationComplete.isHidden = true
            }
               
         if(getSaveValue(value: "acceptButton") == ""){
                    acceptButton.isHidden = false
            }
         if(getSaveValue(value: "acceptButton") == "true"){
                    acceptButton.isHidden = false
            }
         if(getSaveValue(value: "acceptButton") == "false"){
                    acceptButton.isHidden = true
            }
        
    }
    
    func getRequesterData() {
        guard let url = URL(string: "\(baseUrl)api/v1/account/\(acceptPk)") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    let responseModel = try JSONDecoder().decode(ProfileModel.self, from: data)
                    DispatchQueue.main.async {
                        
                        self.userName.text = responseModel.username?.capitalizingFirstLetter()
                        self.contactNumber.text = responseModel.mobileNumber
                        self.bloodGroup.text = responseModel.bloodGroup?.toUpperBG()
                        self.location.text = responseModel.area
                        
                        //let imageUrl = "\(responseModel.user_image!)"
                        //ImageService.downloadImage(url: URL(string: imageUrl)!) { image in
                            //self.imageView.image = image
                        //}
                    }
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        } .resume()
    }
 
    
    
    @IBAction func donationComplete(_ sender: Any) {
        
         let alert = UIAlertController(title: "Confirmation", message: "Do you Want to mark this request as completed? Your Entry will be deleted", preferredStyle: .alert)
               
               alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

               alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                   self.setRequestConfirmedStatusToProfile(param: false, completion: {
                       success in
                       if(success == true){
                       // self.removeSaveValue()
                        self.removeCompletedBloodDonationRequest()
                        
                        DispatchQueue.main.async {
                            self.removeSaveValue()
                            let alert = UIAlertController(title: "Confirmation", message: "Operation completed successfully", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                             
                             self.navigationController?.popToRootViewController(animated: false)
                             self.pushViewControllerExt(storyboardName: "Main", VcIdentifier: "MenuViewController")
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                       }
                       else{
                           self.alertUser(alertTitle: "Not Successful", alertMessage: "Confirmation Failed", actionTitle: "OK")
                       }
                   } )
               
               }))

               self.present(alert, animated: true)
        
    }
    
    func removeCompletedBloodDonationRequest(){
        print("Confirm Pressed :" + notificationID)
        guard let url = URL(string: "\(baseUrl)notification/api/v1/delete/\(notificationID)") else { return }
          var request = URLRequest(url: url)
          request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
          request.httpMethod = "DELETE"
          
        
          
          let parameters: [String: Any] = [:]
          
          request.addValue("application/json", forHTTPHeaderField: "Content-Type")
          
          
          
          do {
              
              request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
          } catch let error {
              print(error.localizedDescription)
          }
          
          URLSession.shared.dataTask(with: request) { data, response, error  in
              
              do {
                  let responseModel = try JSONDecoder().decode(DeleteResponse.self, from: data!)
//                if(responseModel.response == "deleted"){
//
//                }
                
             
//                let alert = UIAlertController(title: "Completed", message: responseModel.response, preferredStyle: .alert)
//                       alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//                           self.pushViewControllerExt(storyboardName: "Main", VcIdentifier: "MenuViewController")
//                       }))
//                       self.present(alert, animated: true, completion: nil)
                   
              }
              catch {
                  print(error)
              }
              
          }.resume()
    }
    
    
    @IBAction func acceptRequestButton(_ sender: Any) {
        
        let alert = UIAlertController(title: "Confirmation", message: "Do you Want to accept this request?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

            self.setRequestConfirmedStatusToProfile(param: true, completion: {
                success in
                if(success == true){
                    
                    self.acceptRequestConfirmDone()
                    self.deleteCurrentRequestList()
                    
                    
                }
                else{
                    self.alertUser(alertTitle: "Not Successful", alertMessage: "Confirmation Failed", actionTitle: "OK")
                }
                
                DispatchQueue.main.async {

                    self.saveValue(state1: "false", state2: "true")
                }
                
            } )
        
        }))

        self.present(alert, animated: true)
        
    }
    

    func deleteCurrentRequestList(){
        print("Confirm Pressed :" + notificationID)
          guard let url = URL(string: "\(baseUrl)notification/api/v1/delete-request/\(pk ?? "0")") else { return }
          var request = URLRequest(url: url)
          request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
          request.httpMethod = "DELETE"
          
        
          
          let parameters: [String: Any] = [:]
          
          request.addValue("application/json", forHTTPHeaderField: "Content-Type")
          
          
          
          do {
              
              request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
          } catch let error {
              print(error.localizedDescription)
          }
          
          URLSession.shared.dataTask(with: request) { data, response, error  in
              
              do {
                  let responseModel = try JSONDecoder().decode(DeleteResponse.self, from: data!)
                  
              }
              catch {
                  print(error)
              }
              
          }.resume()
    
    }
    
    func acceptRequestConfirmDone(){
        
        print("Confirm Pressed :" + notificationID)
        guard let url = URL(string: "\(baseUrl)notification/api/v1/update/\(notificationID)") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "PUT"
        
      
        
        let parameters: [String: Any] = [
            "status" : "True"
        ]
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        print(parameters)
        
        do {
            
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch let error {
            print(error.localizedDescription)
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error  in
            
            do {
                let responseModel = try JSONDecoder().decode(AcceptRequestConfirm.self, from: data!)
                
                let confirmResponse = responseModel.response
                
                //print(confirmResponse)
                

                if confirmResponse == "updated"
                {
                    DispatchQueue.main.async {
                        self.alertUser(alertTitle: "Successful", alertMessage: "You Accepted The Request", actionTitle: "OK")
                    }
                    
                }
                else {
                    
                    DispatchQueue.main.async {
                        self.alertUser(alertTitle: "Not Successful", alertMessage: "Confirmation Failed", actionTitle: "OK")
                    }
                }
            }
            catch {
                print(error)
            }
            
        }.resume()
   
    }
    
    
    func setRequestConfirmedStatusToProfile(param: Bool, completion: @escaping (_ success: Bool) -> ()){
               var success = true
               guard let url = URL(string: "\(baseUrl)api/v1/account/notificationupdate/\(pk ?? "0")") else { return }
               var request = URLRequest(url: url)
               request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
               request.httpMethod = "PUT"
               
         
               let parameters: [String: Any] = [
                   "status" : "\(param)"
               ]
               
               request.setValue("application/json", forHTTPHeaderField: "Content-Type")
               
               print(parameters)
               
               do {
                   
                   request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
               } catch let error {
                   print(error.localizedDescription)
               }
               
               URLSession.shared.dataTask(with: request) { data, response, error  in
                   
                   do {
                       let responseModel = try JSONDecoder().decode(AcceptRequestConfirm.self, from: data!)
                       
                       let confirmResponse = responseModel.response
                       
                        if confirmResponse == "updated"
                           {
                                success = true
                                completion(success)
                            }
                            else {
                                 success = false
                                 completion(success)
                            }
                    }
                   catch {
                       print(error)
                   }
                
               }.resume()
        
    }
    
    
     func alertUser(alertTitle: String, alertMessage: String, actionTitle: String) {
           let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
           alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { (action) in
            
            self.navigationController?.popToRootViewController(animated: false)
            
            self.pushViewControllerExt(storyboardName: "CurrentRequest", VcIdentifier: "CurrentRequest")
           }))
           self.present(alert, animated: true, completion: nil)
       }
    
}
