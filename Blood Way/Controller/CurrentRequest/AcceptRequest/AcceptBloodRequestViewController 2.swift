import UIKit

class AcceptBloodRequestViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var contactNumber: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var bloodGroup: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    
    var acceptPk = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.layer.cornerRadius = 15
        acceptButton.layer.cornerRadius = 20

        getRequesterData()
    }
    
    func getRequesterData() {
        
        //demo data from user profile
        guard let url = URL(string: "\(baseUrl)/api/v1/account/\(acceptPk)") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token 685dc509e52d34adbb8df473d8ab959a35fd7ba5", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    let responseModel = try JSONDecoder().decode(ProfileModel.self, from: data)
                    DispatchQueue.main.async {
                        
                        self.userName.text = responseModel.username
                        self.contactNumber.text = "Phone: \(responseModel.mobile_number!)"
                        self.bloodGroup.text = responseModel.blood_group
                        self.location.text = "Area: \(responseModel.area!)"
                        
                        //let imageUrl = "\(responseModel.user_image!)"
                        //ImageService.downloadImage(url: URL(string: imageUrl)!) { image in
                            //self.imageView.image = image
                        //}
                    }
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        } .resume()
    }
    
    @IBOutlet weak var promotionalText: UILabel!
    @IBOutlet weak var distanceFromContainerViewToTop: NSLayoutConstraint!
    
    @IBAction func acceptRequestButton(_ sender: Any) {
        
        //acceptButton.isEnabled = true
        acceptButton.isHidden = true
        promotionalText.text = "You have accepted this request"
        distanceFromContainerViewToTop.constant = 100
    }
    
}
