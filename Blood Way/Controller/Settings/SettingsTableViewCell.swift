//
//  ManageAccountTableViewCell.swift
//  Blood Way
//
//  Created by Fahim Rahman on 19/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
