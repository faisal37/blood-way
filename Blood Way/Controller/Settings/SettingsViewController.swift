//
//  ManageAccountViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 19/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

struct LogoutResponse:Codable{
    let detail:String?
}

class SettingsViewController: UIViewController {

    @IBOutlet weak var settingsTableView: UITableView!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var groupLabel: UILabel!
    
    
    let generalSection = ["Update Information","Change Password"]
    
    let moreSection = ["Privacy Policy","FAQ","About Us","Log Out"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let img = UIImage(named: "menuSquareImg")
        navigationController?.navigationBar.setBackgroundImage(img, for: .default)

        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Settings"
        
        navigationController?.navigationBar.barTintColor = .white
       
        //customNavBar()
        loadProfileData()
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        settingsTableView.tableFooterView = UIView()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Settings"
    }
    
    func customNavBar(){
        let img = UIImage(named: "menuSquareImg")
        navigationController?.navigationBar.setBackgroundImage(img, for: .default)
        
          self.title = "Settings"
        navigationController?.navigationBar.backItem?.title = "Menu"
        
        navigationController?.navigationBar.barTintColor = .white
        //setCustomNavigationBar(backTitle: "Menu", barTitle: "Settings", barTintColor: .white)
        
    }
    
}

  

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return generalSection.count
        }
        else {
            return moreSection.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SettingsTableViewCell

        if indexPath.section == 0 {
            cell.label.text = generalSection[indexPath.row]
        }
        
        else {
            cell.label.text = moreSection[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "General"
        }
        else {
            return "More"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                print("Update Information")
                
                pushViewController(storyboardName: "SettingsDetails", VcIdentifier: "UpdateInfoViewController")
            }
            if indexPath.row == 1 {
                print("Change Password")
                
                pushViewController(storyboardName: "SettingsDetails", VcIdentifier: "ChangePasswordViewController")
            }
        }
        
        else {
            
            if indexPath.row == 0 {
                print("Privacy Policy")
                
                pushViewController(storyboardName: "SettingsDetails", VcIdentifier: "privacyViewController")
            }
            if indexPath.row == 1 {
                print("FAQ")
                
                pushViewController(storyboardName: "SettingsDetails", VcIdentifier: "faqViewController")
            }
            if indexPath.row == 2 {
                print("About Us")
                
                pushViewController(storyboardName: "SettingsDetails", VcIdentifier: "aboutViewController")
            }
            if indexPath.row == 3 {
                print("Log Out")
                
                logoutConfirmRequest() //
                
            }
        }
    }
    
    
    func pushViewController(storyboardName: String, VcIdentifier: String) {
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: VcIdentifier)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func logoutConfirmRequest(){
        
      let alert = UIAlertController(title: "Confirmation", message: "Want to logout from the  app now?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

           
            self.logoutConfirmDone()
            self.removeSaveValue()
            
        }))

        self.present(alert, animated: true)
        
    }
    
    
    func logoutConfirmDone(){
        guard let url = URL(string: "\(baseUrl)api/v1/rest-auth/logout/") else { return }
        var request = URLRequest(url: url)
        
        request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "POST"
        
        let parameters: [String: Any] = [:]
        
       
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: []) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let data = data {
                do {
                    let responseModel = try JSONDecoder().decode(LogoutResponse.self, from: data)
                    
                    print(responseModel)
                    
                    UserDefaults.standard.setValueForLogin(value: false)
                    self.removeToken()
                   
//                    guard let httpResponse = response as? HTTPURLResponse else {
//                        return
//                    }
//
//                    if httpResponse.statusCode == 200 {
//                        print("http status")
//                    }
                    
                    DispatchQueue.main.async {
                      
                        self.alertUser(alertTitle: "Confirm", alertMessage: responseModel.detail ?? "", actionTitle: "OK")
                    }
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        }.resume()
    }
    
    
    func loadProfileData() {
            
            guard let url = URL(string: "\(baseUrl)api/v1/account/\(pk!)") else { return }
            
            var request = URLRequest(url: url)
            request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
            request.httpMethod = "GET"
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let data = data {
                    do {
                        
                        let responseModel = try JSONDecoder().decode(ProfileModel2.self, from: data)
                        print(responseModel)
                        
                        print("here from call")
                        
                        
                        DispatchQueue.main.async {
                            

                            self.nameLabel.text = responseModel.username?.capitalizingFirstLetter()
                            self.groupLabel.text = responseModel.blood_group?.toUpperBG()
                            self.emailLabel.text =  responseModel.email ?? ""
                            
                            
//                        print(responseModel.user_image)
//
//                            let img = responseModel.user_image
//
//                            //image load
//                            //let imageUrl = \(baseUrl)\(img)
//
//                            ImageService.downloadImage(url: URL(string: img ?? "")!) { image in
//                            self.profileImage.image = image
//                            }
                        }
                    }
                        
                    catch {
                        print(error.localizedDescription)
                        return
                    }
                }
            } .resume()
        }
        
    
    func alertUser(alertTitle: String, alertMessage: String, actionTitle: String) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { action in
            self.pushViewController(storyboardName: "Main", VcIdentifier: "AuthViewController")
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
