//
//  ChangePasswordViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 10/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var newPassword2TextField: UITextField!
    @IBOutlet weak var newPassword1TextField: UITextField!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var confirmButton: UIButton!
    
    //var responseData = [pwd]()
    var errorResponse = [String]()
    var successResponse = String()//"Something Went Wrong"
    var test = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Change Password"
        self.oldPasswordTextField.becomeFirstResponder()
        oldPasswordTextField.text = ""
        
        self.newPassword1TextField.becomeFirstResponder()
        newPassword1TextField.text = ""
        
        self.newPassword2TextField.becomeFirstResponder()
        newPassword2TextField.text = ""
        // Do any additional setup after loading the view.
        
        confirmButton.layer.cornerRadius = 25
        
    }
    
    @IBAction func passwordChangeConfirm(_ sender: Any) {
        if(newPassword1TextField.text==newPassword2TextField.text){
            sendPasswordChangeRequest()
        }else{
            self.alertUser(alertTitle: "ERROR!", alertMessage: "Password Confirmation Failed.", actionTitle: "OK")
            
        }
        
    }
    
    func sendPasswordChangeRequest(){
        guard let url = URL(string: "\(baseUrl)api/v1/rest-auth/password/change/") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        let parameters: [String: Any] = [
            "old_password" : "\(oldPasswordTextField.text!)",
            "new_password1" : "\(newPassword1TextField.text!)",
            "new_password2" : "\(newPassword2TextField.text!)"
        ]
       
        //["email" : "faisal@gmail.com"]
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: []) // pass dictionary to nsdata object and set it as request body
        } catch let error {
            print(error.localizedDescription)
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            if let data = data {
                do {
                    let responseModel = try JSONDecoder().decode(pwd.self, from: data)
                     DispatchQueue.main.async {
                        print(responseModel)
                        self.errorResponse = responseModel.err ?? [""]
                        self.successResponse = responseModel.success
                        
                        
                        var msg = ""
                        
                        msg = self.errorResponse[0] + " Please enter strong password."
                        
                        
                        if(self.successResponse=="New password has been saved."){
                            self.alertUser(alertTitle: "Success", alertMessage: "New password has been saved.", actionTitle: "OK")
                        }
                        else{
                            self.alertUser(alertTitle: "ERROR!", alertMessage: msg, actionTitle: "OK")
                            
                        }
                    
                        
                    }
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        }.resume()
        
    }
    
    //##############send alert
     func alertUser(alertTitle: String, alertMessage: String, actionTitle: String) {
               let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
               alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { (action) in
               }))
               self.present(alert, animated: true, completion: nil)
           }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
