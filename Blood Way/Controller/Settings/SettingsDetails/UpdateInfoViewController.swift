//
//  ViewController.swift
//  Setting Details
//
//  Created by Arman Akash on 9/3/20.
//  Copyright © 2020 Arman Akash. All rights reserved.
//


//
//  SignUpViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 6/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UpdateInfoViewController: UIViewController, UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
    
    var responseModel = [SignUpResponseModel]()
    // Outlets
    
    @IBOutlet weak var scrollView: UIScrollView!
    

    @IBOutlet weak var updateButton: UIButton!
    
    @IBOutlet weak var emailTextField: UnderlinedTextField!
    @IBOutlet weak var userNameTextField: UnderlinedTextField!
    @IBOutlet weak var areaTextField: UnderlinedTextField!
    @IBOutlet weak var cityTextField: UnderlinedTextField!
    @IBOutlet weak var BloodGroupTextField: UnderlinedTextField!
    @IBOutlet weak var weightTextField: UnderlinedTextField!
    @IBOutlet weak var genderTextField: UnderlinedTextField!
    @IBOutlet weak var ageTextField: UnderlinedTextField!
    @IBOutlet weak var phoneNumberTextField: UnderlinedTextField!

    
    let citiesPickerView = UIPickerView()
    let genderPickerView = UIPickerView()
    
    // Variables
    
    var email: String?
    var userName: String?
    var area: String?
    var city: String?
    var bloodGroup: String?
    var weight: String?
    var gender: String?
    var age: String?
    var phone: String?

    
    var selectedCities: String?
    
    var cities = [citiesModel]()
    
    //var cities = ["uttara","mohammadpur","gulshan","banani"]
    var genders = ["Male","Female","Other"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.backItem?.title = "Settings"
        self.title = "Update Info"
        scrollView.delegate = self
        updateButton.layer.cornerRadius = 25
        
        self.emailTextField.becomeFirstResponder()
        
        getCities()
        picker()
        toolBar()
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Settings"
        self.title = "Update Info"
    }
    
    func picker() {
        
        citiesPickerView.delegate = self
        genderPickerView.delegate = self
        cityTextField.inputView = citiesPickerView
        genderTextField.inputView = genderPickerView
        citiesPickerView.backgroundColor = .white
        genderPickerView.backgroundColor = .white
    }
    
    // Creating toolbar
    
    func toolBar() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePressed))
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
    
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: SignUpViewController.self, action: nil)
        toolBar.setItems([doneButton, cancelButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        toolBar.barTintColor = .systemPink
        toolBar.tintColor = .white
        toolBar.items = [doneButton, flexibleSpace, cancelButton]
        
        cityTextField.inputAccessoryView = toolBar
        genderTextField.inputAccessoryView = toolBar
    }
    
    @objc func donePressed() {
        view.endEditing(true)
    }
    
    @objc func cancelPressed() {
        cityTextField.text = ""
        genderTextField.text = ""
        view.endEditing(true)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == citiesPickerView {
            return cities.count
        }
        if pickerView == genderPickerView {
            return genders.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == citiesPickerView {
            return cities[row].locationName
        }
        if pickerView == genderPickerView {
            return genders[row]
        }
        
        return ""
        //return cities[row].locationName
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == citiesPickerView {
            city = "\(cities[row].id!)"
            cityTextField.text = cities[row].locationName
        }
        if  pickerView == genderPickerView {
            gender = genders[row]
            genderTextField.text = genders[row]
        }
            
            //city = "\(cities[row].id!)"
            //cityTextField.text = cities[row].locationName
    }
    
    
    // Sign Up Button
    @IBAction func updateButton(_ sender: UIButton) {
        
        if email != "" && userName != "" && area != "" && city != "" && bloodGroup != "" && age != "" && phone != "" {
        email = emailTextField.text
        userName = userNameTextField.text
        area = areaTextField.text
        
        bloodGroup = BloodGroupTextField.text
        gender = genderTextField.text
        weight = weightTextField.text
        age = ageTextField.text
        phone = phoneNumberTextField.text
        
            
       // signUpMethod()
        
        }
    }
    
    
    // Get cities from API
    
    func getCities() {
        
        guard let url = URL(string: "\(baseUrl)location/api/v1/list") else { return }
        //print(url)
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let data = data, error == nil {
                do {
                    
                    self.cities = try JSONDecoder().decode([citiesModel].self, from: data)
                   // print(self.cities)
                    
                }
                catch {
                    print("error")
                }
            }
        }.resume()
    }
    
    
    // Post sign up credentials to the server
    
    func signUpMethod() {
        
//        let signUpParameters = SignUpRequestModel(email: "\(email!)", username: "\(userName!)", area: "\(area!)", city: "\(city!)", blood_group: "\(bloodGroup!)", weight: "\(weight!)", gender: "\(gender!)", age: "\(age!)", mobile_number: "\(phone!)")
        
       // let headers: HTTPHeaders = ["Accept": "application/json"]
        
//        AF.request("\(baseUrl)api/v1/account/register",
//                   method: .post,
//                   parameters: signUpParameters
//                   ).response { response in
//
//                    let items = JSON(response.value as Any)
//
//                    DispatchQueue.main.async {
//
//                        let model = SignUpResponseModel(items)
//                        token = model.token!
//
//                        print(model)
//
//                        if model.response! == "successfully registered new user." {
//
//                            let vc: LoginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//                            self.navigationController?.pushViewController(vc, animated: true)
//
//                        }
//                        else {
//                            print("Error! couldn't added new user!")
//                            self.phoneNumberTextField.text = ""
//                            self.passwordTextField.text = ""
//                        }
//
//                            debugPrint(response)
//                    }
//        }.resume()
    }
    
    
    // Only Vertical Scroll enabled
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
}

