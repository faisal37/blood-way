//
//  SentRequestViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 29/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class SentRequestViewController: UIViewController {
    
    var responseData = [AcceptedUserModel]()
    var senderID = String()
    

    @IBOutlet weak var sentRequestTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Sent Requests"
        
        sentRequestTableView.delegate = self
        sentRequestTableView.dataSource = self
        sentRequestTableView.tableFooterView = UIView()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Clear List", style: .plain, target: self, action: #selector(clearSentRequestList))
        getSentRequestList()
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Sent Requests"
    }
    
    @objc func clearSentRequestList(){
        let alert = UIAlertController(title: "Confirm", message: "Do you want to clear sent request list?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action) in
            self.clearSentRequestListAction()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func clearSentRequestListAction(){
        
        
              guard let url = URL(string: "\(baseUrl)notification/api/v1/delete-sent-request-list/\(pk ?? "0")") else { return }
              var request = URLRequest(url: url)
              request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
              request.httpMethod = "DELETE"
              
            
              
              let parameters: [String: Any] = [:]
              
              request.addValue("application/json", forHTTPHeaderField: "Content-Type")
              
              
              
              do {
                  
                  request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
              } catch let error {
                  print(error.localizedDescription)
              }
              
              URLSession.shared.dataTask(with: request) { data, response, error  in
                  
                  do {
                      let responseModel = try JSONDecoder().decode(DeleteResponseConfirm.self, from: data!)
                      
                    DispatchQueue.main.async {
                        self.alertUser(alertTitle: "Confirmation", alertMessage: "Operation Completed Successfully", actionTitle: "OK")
                    }
                    
                  }
                  catch {
                      print(error)
                  }
                  
              }.resume()
        
    }
    
    
    func getSentRequestList(){
        
    guard let url = URL(string: "\(baseUrl)notification/api/v1/getlist?senderid=\(pk!)") else { return }
          var request = URLRequest(url: url)
          request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
          request.httpMethod = "GET"
          //print(token!)
          URLSession.shared.dataTask(with: request) { (data, response, error) in
              
              if let data = data {
                  do {
                      
                      let responseModel = try JSONDecoder().decode([AcceptedUserModel].self, from: data)
                      
                      print("current request test")
                      print(responseModel)
                    
                   self.responseData = responseModel
                    
                      DispatchQueue.main.async {
                       
                        self.sentRequestTableView.reloadData()
                      
                      }
                  }
                  catch {
                      print(error.localizedDescription)
                      return
                  }
              }
          } .resume()
      }
    
    func alertUser(alertTitle: String, alertMessage: String, actionTitle: String) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { (action) in
         self.navigationController?.popToRootViewController(animated: false)
         
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
}




extension SentRequestViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(responseData.count)
        return responseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SentRequestTableViewCell = sentRequestTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SentRequestTableViewCell
        
        cell.nameLabel.text = responseData[indexPath.row].receverName?.capitalizingFirstLetter()
        cell.phoneLabel.text = responseData[indexPath.row].receverNumber
        cell.groupLabel.text = responseData[indexPath.row].receverBloodGroup?.toUpperBG()
        cell.locationLabel.text = responseData[indexPath.row].receverLocation
        
        return cell
    }
    
}
