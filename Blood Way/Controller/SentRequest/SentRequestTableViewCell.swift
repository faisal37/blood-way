//
//  SentRequestTableViewCell.swift
//  Blood Way
//
//  Created by Fahim Rahman on 29/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class SentRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellContentView.layer.cornerRadius = 15
        profilePicture.layer.cornerRadius = profilePicture.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
