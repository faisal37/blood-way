//
//  MenuViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 8/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    // Outlets
    
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var donateButton: UIButton!
    @IBOutlet weak var currentRequestButton: UIButton!
    @IBOutlet weak var acceptRequestButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeButtons()
        customizingNavBarItems()
    }
    
    @IBAction func requestForBlood(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "RequestViewController")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func currentRequestButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "CurrentRequest", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CurrentRequest")
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func profileButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func AcceptedRequest(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "AcceptedRequest", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AcceptedRequest")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}


// Extention of the Menu View Controller

extension MenuViewController {
    
    // SettingBarItem
    
    func customizingNavBarItems() {
        
        navigationItem.title = "Dashboard"
        
        navigationItem.hidesBackButton = true
        
        let editBarButtonItem = UIBarButtonItem(title: "Setting", style: .plain, target: self, action: #selector(setting))
        navigationItem.rightBarButtonItem  = editBarButtonItem
        editBarButtonItem.tintColor = .white
    }
    
    
    
    @objc func setting() {
        
        let storyboard = UIStoryboard(name: "Setting", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SettingViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    // Customizing all the buttons of the main menu view.
    func customizeButtons()  {
        
        requestButton.clipsToBounds = true
        donateButton.clipsToBounds = true
        currentRequestButton.clipsToBounds = true
        acceptRequestButton.clipsToBounds = true
        
        requestButton.layer.borderWidth = 1
        donateButton.layer.borderWidth = 1
        currentRequestButton.layer.borderWidth = 1
        acceptRequestButton.layer.borderWidth = 1
        
        requestButton.layer.cornerRadius = 35
        donateButton.layer.cornerRadius = 35
        currentRequestButton.layer.cornerRadius = 35
        acceptRequestButton.layer.cornerRadius = 35
    }
    
    // Only Vertical Scroll enabled
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
}
