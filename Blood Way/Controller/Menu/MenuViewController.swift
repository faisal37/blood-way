//
//  MenuViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 8/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit




class MenuViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // Outlets
    
    @IBOutlet weak var menuUpImage: UIImageView!
    @IBOutlet weak var requestButton: UIButton!
    @IBOutlet weak var acceptedButton: UIButton!
    @IBOutlet weak var currentRequestButton: UIButton!
    @IBOutlet weak var sentButton: UIButton!
    @IBOutlet weak var profile: UIButton!
    @IBOutlet weak var settings: UIButton!
    
    
    override func viewDidLoad() {
        
        //navigationController?.setNavigationBarHidden(false, animated: true)
         
        if(UserDefaults.standard.getValueofLogin()==true){
              super.viewDidLoad()
              self.navigationItem.hidesBackButton = true
              customizingView()
            
            token = getToken(value: "Token")
            pk = getToken(value: "PK")
            
        }
        else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
            navigationController?.pushViewController(vc!, animated: true)
         }
        
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    
    @IBAction func requestForBlood(_ sender: UIButton) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "RequestViewController")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    @IBAction func currentRequestButton(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "CurrentRequest", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CurrentRequest")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func acceptedButton(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "AcceptedRequest", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AcceptedRequestViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func sentRequest(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "SentRequest", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SentRequestViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func profileAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Profile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProfileViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func settingAction(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}




// Extention of the Menu View Controller

extension MenuViewController {
    
    // Customizing all the buttons of the main menu view.
    func customizingView() {
        
        requestButton.clipsToBounds = true
        acceptedButton.clipsToBounds = true
        currentRequestButton.clipsToBounds = true
        sentButton.clipsToBounds = true
        profile.clipsToBounds = true
        settings.clipsToBounds = true
        
        requestButton.layer.borderWidth = 0.5
        acceptedButton.layer.borderWidth = 0.5
        currentRequestButton.layer.borderWidth = 0.5
        sentButton.layer.borderWidth = 0.5
        profile.layer.borderWidth = 0.5
        settings.layer.borderWidth = 0.5
        
        requestButton.layer.cornerRadius = 40//requestButton.frame.height / 2
        acceptedButton.layer.cornerRadius = 40//acceptedButton.frame.height / 2
        currentRequestButton.layer.cornerRadius = 40//currentRequestButton.frame.height / 2
        sentButton.layer.cornerRadius = 40//sentButton.frame.height / 2
        profile.layer.cornerRadius = 40//profile.frame.height / 2
        settings.layer.cornerRadius = 40//settings.frame.height / 2
        
        requestButton.layer.borderColor = UIColor.systemPink.cgColor
        acceptedButton.layer.borderColor = UIColor.systemPink.cgColor
        currentRequestButton.layer.borderColor = UIColor.systemPink.cgColor
        sentButton.layer.borderColor = UIColor.systemPink.cgColor
        profile.layer.borderColor = UIColor.systemPink.cgColor
        settings.layer.borderColor = UIColor.systemPink.cgColor
        
        menuUpImage.layer.cornerRadius = 30
        menuUpImage.layer.masksToBounds = true
        menuUpImage.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner,]
        
    }
    
    // Only Vertical Scroll enabled
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
}
