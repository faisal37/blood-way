//
//  LocationFilterViewController.swift
//  Blood Way
//
//  Created by Fahim Rahman on 29/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class LocationFilterViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }

    @IBOutlet weak var locationFilterTableView: UITableView!
    
    var cityModel = [citiesModel]()
    var filterCities:[String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = "Request Blood"
        self.title = "Filter Location"

        locationFilterTableView.delegate = self
        locationFilterTableView.dataSource = self
        
        getFiltureCities()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Request Blood"
        self.title = "Filter Location"
    }
    
    func getFiltureCities() {
        
        guard let url = URL(string: "\(baseUrl)location/api/v1/list") else { return }
        //print(url)
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let data = data, error == nil {
                do {
                    
                    self.cityModel = try JSONDecoder().decode([citiesModel].self, from: data)
                    
                    for cities in self.cityModel {
                        self.filterCities.append(cities.locationName!)
                    }
                    print(self.filterCities)
                    
                }
                catch {
                    print("error")
                }
            }
        }.resume()
    }
}

extension LocationFilterViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: LocationFilterTableViewCell = locationFilterTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LocationFilterTableViewCell
        
        cell.locationLabel.text = self.filterCities[indexPath.row].uppercased()
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
