//
//  LocationFilterTableViewCell.swift
//  Blood Way
//
//  Created by Fahim Rahman on 29/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class LocationFilterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
