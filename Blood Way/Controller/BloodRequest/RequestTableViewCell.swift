//
//  RequestTableViewCell.swift
//  Blood Way
//
//  Created by Fahim Rahman on 11/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class RequestTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cellContentView: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var groupLabel: UILabel!
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellContentView.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    public func configure(with json: RequestForBlood) {
        
        nameLabel.text = json.username?.capitalizingFirstLetter() ?? ""
        phoneLabel.text = json.mobileNumber
        addressLabel.text = json.area?.capitalizingFirstLetter() ?? ""
        //addressLabel.text = json.city ?? ""
        groupLabel.text = json.bloodGroup?.toUpperBG() ?? ""
        
    }
    
}

