//

//  Blood Way
//
//  Created by Fahim Rahman on 11/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RequestViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
    
    @IBOutlet weak var requestTableView: UITableView!
    
    var requestForBlood = [RequestForBlood]()
    
    var requestURL = "\(baseUrl)api/v1/account/list?blood_group="
    
    //let requestUrl = "\(baseUrl)api/v1/account/list?search="
    
    @IBOutlet weak var aP: UIButton!
    @IBOutlet weak var bP: UIButton!
    @IBOutlet weak var abP: UIButton!
    @IBOutlet weak var oP: UIButton!
    @IBOutlet weak var aN: UIButton!
    @IBOutlet weak var bN: UIButton!
    @IBOutlet weak var abN: UIButton!
    @IBOutlet weak var oN: UIButton!
    var bloodbutton:[UIButton] = [UIButton]()
    //var self.bloodbutton = [self.aP,self.bP,self.abP,self.oP,self.aN,self.bN,self.abN,self.oN]
    
    @IBOutlet weak var search: UIButton!
    
    
    var bloodGroup: String? = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Blood Request"
        self.bloodbutton = [self.aP,self.bP,self.abP,self.oP,self.aN,self.bN,self.abN,self.oN]
        requestTableView.delegate = self
        requestTableView.dataSource = self
        
        designingButtonsBG()
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Location", style: .plain, target: self, action: #selector(locationFilter))
        
        getData(requestURL: "\(baseUrl)api/v1/account/list", bloodGroup: "")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Blood Request"
    }
    
    var buttonarray = ["a","b","ab","o","a-","b-","ab-","o-"]
    
    var buttonColorChanging = true
    
    @IBAction func bloodGroupbutton(_ sender: UIButton) {
        bloodGroup = ""
        self.requestForBlood.removeAll()
        //let indexbutton = sender.tag
        bloodGroup = buttonarray[sender.tag]
        
        // buttonColorChanging = !buttonColorChanging
        
        for button in bloodbutton{
            
            if button == sender{
                button.backgroundColor = UIColor.systemPink
                button.setTitleColor(.white, for: .normal)
                
            }
            else {
                button.backgroundColor = UIColor.white
                button.setTitleColor(.systemPink, for: .normal)
            }
        }
    }
    
    
    
    let headers: HTTPHeaders = ["Authorization": "Token \(token!)"]
    
    @IBAction func searchButton(_ sender: UIButton) {
        if bloodGroup != "" {
            getData(requestURL: "\(requestURL)", bloodGroup: "\(bloodGroup!)")
            bloodGroup = ""
            requestTableView.reloadData()
        }
    }
    
    
    
    func getData(requestURL: String, bloodGroup: String) {
        
        AF.request("\(requestURL)\(bloodGroup)",
            method: .get, headers: headers
        ).response { response in
            debugPrint(response)
            
            let items = JSON(response.value as Any)
            
            print(items.count)
            
//            if(items.count == 0){
//                let alert = UIAlertController(title: "Result", message: "No User Found", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//                    self.pushViewControllerExt(storyboardName: "Main", VcIdentifier: "RequestViewController")
//                }))
//                self.present(alert, animated: false, completion: nil)
//            }
            
            DispatchQueue.main.async() {
                
                for item in items {
                    
                    let data = RequestForBlood(item.1)
                    self.requestForBlood.append(data)
                    self.requestTableView.reloadData()
                }
            }
        }.resume()
        self.requestForBlood.removeAll()
    }
    
    
    @objc func locationFilter() {
        let storyboard = UIStoryboard(name: "LocationFilter", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LocationFilterViewController") as!  LocationFilterViewController
        navigationController?.pushViewController(vc, animated: true)
        

    }
}



extension RequestViewController: UITableViewDelegate,  UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        requestForBlood.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: RequestTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RequestTableViewCell
        cell.configure(with: requestForBlood[indexPath.row])
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let vc:SendRequestViewController = storyboard?.instantiateViewController(withIdentifier: "SendRequestViewController") as! SendRequestViewController
        
        vc.recieverId = requestForBlood[indexPath.row].pk!
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func designingButtonsBG() {
        
        aP.layer.cornerRadius = 15
        bP.layer.cornerRadius = 15
        abP.layer.cornerRadius = 15
        oP.layer.cornerRadius = 15
        aN.layer.cornerRadius = 15
        bN.layer.cornerRadius = 15
        abN.layer.cornerRadius = 15
        oN.layer.cornerRadius = 15
        search.layer.cornerRadius = 10
        
    }
}
