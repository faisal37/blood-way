//
//  Blood Way
//
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class SendRequestViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
    
    //var pk = String()
    var recieverId = String()
    var senderId = String()
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var bloodGroupLabel: UILabel!
    @IBOutlet weak var confirm: UIButton!
     //###########################################################
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = "Blood Request"
        self.title = "Send Request"
        confirm.layer.cornerRadius = 25
        imgView.layer.cornerRadius = imgView.frame.height / 2
        getProfile()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Blood Request"
        self.title = "Send Request"
    }
    
    
    @IBAction func confirm(_ sender: UIButton) {
        confirmSend()
    }
//###################################################################
    func getProfile() {
        
        guard let url = URL(string: "\(baseUrl)api/v1/account/\(recieverId)") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    let responseModel = try JSONDecoder().decode(ProfileModel.self, from: data)
                    
                    print(responseModel)
                    
                    DispatchQueue.main.async {
                        
                        self.gettingProfileModelDataInsert(with: responseModel)
                        
                    }
         
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        } .resume()
    }
    
    //###################################################################
    
    func confirmSend() {
        
        print("Confirm Pressed")
        guard let url = URL(string: "\(baseUrl)notification/api/v1/create") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        senderId = getToken(value: "PK")
        
        print(recieverId)
        print(senderId)
        
        let parameters: [String: Any] = [
            "receverid" : "\(recieverId)",
            "senderid" : "\(senderId)"
        ]
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        print(parameters)
        
        do {
            
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch let error {
            print(error.localizedDescription)
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error  in
            
            do {
                let responseModel = try JSONDecoder().decode(SendReqConfirmModel.self, from: data!)
                
                let confirmResponse = responseModel.response
                
                print(confirmResponse)
                
                if confirmResponse == "created"
                {
                    DispatchQueue.main.async {
                        self.alertUser(alertTitle: "Successful", alertMessage: "Send Request Done", actionTitle: "OK")
                    }
                    
                }
                else {
                    
                    DispatchQueue.main.async {
                        self.alertUser(alertTitle: "Not Successful", alertMessage: "Send Request Failed", actionTitle: "OK")
                    }
                }
            }
            catch {
                print(error)
            }
            
        }.resume()
    }
     //###########################################################
    func alertUser(alertTitle: String, alertMessage: String, actionTitle: String) {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { (action) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
 //################################################################
extension SendRequestViewController{
    
    func gettingProfileModelDataInsert(with responseModel:ProfileModel)
    {
        
        self.nameLabel.text = responseModel.username?.capitalizingFirstLetter() ?? ""
        self.phoneLabel.text = responseModel.mobileNumber
        self.addressLabel.text = responseModel.area?.capitalizingFirstLetter() ?? ""
        self.bloodGroupLabel.text = responseModel.bloodGroup?.toUpperBG() ?? ""
        
//        self.recieverId = responseModel.pk ?? 0
//        self.senderId = Int(pk) ?? 0
//
//        print(re)
//        print(senderId)
        
//        if responseModel.receverinfo == nil {
//            for i in 1...responseModel.receverinfo!.count - 1 {
//
//                self.recieverId = responseModel.receverinfo![i].receverid!
//                self.senderId = responseModel.receverinfo![i].senderid!
//            }
            
            //let imageUrl = "\(responseModel.user_image!)"
            //ImageService.downloadImage(url: URL(string: imageUrl)!) { image in
            //self.imageView.image = image
            //}
//        }
    }
}
