//
//  ConfirmViewController.swift
//  Blood Way
//
//  Created by Arman Akash on 28/1/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class ConfirmRequestViewController: UIViewController {
    
    var pk = String()
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var bloodGroupLabel: UILabel!
    
    @IBOutlet weak var confirm: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirm.layer.cornerRadius = 10
        
        getProfile()
    }
    
    
    @IBAction func confirm(_ sender: Any) {
        
    }
    
    func getProfile() {
        
        guard let url = URL(string: "\(baseUrl)api/v1/account/\(pk)") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token 685dc509e52d34adbb8df473d8ab959a35fd7ba5", forHTTPHeaderField: "Authorization")
        
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    let responseModel = try JSONDecoder().decode(ProfileModel.self, from: data)
                    
                    DispatchQueue.main.async {
                        
                        self.nameLabel.text = responseModel.username
                        self.phoneLabel.text = responseModel.mobile_number
                        self.addressLabel.text = responseModel.area
                        self.bloodGroupLabel.text = responseModel.blood_group
                    }
                        
                        //let imageUrl = "\(responseModel.user_image!)"
                        //ImageService.downloadImage(url: URL(string: imageUrl)!) { image in
                        //self.imageView.image = image
                        //}
                    
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        } .resume()
    }
}
