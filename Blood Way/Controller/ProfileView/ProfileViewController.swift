
import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var areaLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var designImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Profile"
        customizingView()
        getProfileData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Profile"
    }
    
    
    func getProfileData() {
        
        guard let url = URL(string: "\(baseUrl)api/v1/account/\(pk!)") else { return }
        
        var request = URLRequest(url: url)
        request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    let responseModel = try JSONDecoder().decode(ProfileModel.self, from: data)
                    print(responseModel)
                    
                    print("here from call")
                    
                    
                    DispatchQueue.main.async {
                        

                        self.nameLabel.text = responseModel.username?.capitalizingFirstLetter() ?? "" //.capitalizingFirstLetter()
                        self.groupLabel.text = responseModel.bloodGroup?.toUpperBG() ?? ""
                        self.genderLabel.text = "Gender: \(responseModel.gender ?? "".capitalizingFirstLetter())"
                        self.weightLabel.text = "Weight: \(responseModel.weight ?? "".capitalizingFirstLetter())"
                        self.areaLabel.text = "\(responseModel.area?.capitalizingFirstLetter() ?? "")"
                        //self.cityLabel.text =  "\(responseModel.city?.capitalizingFirstLetter())"
                        self.emailLabel.text =  responseModel.email
                        self.phoneLabel.text =  "\(responseModel.mobileNumber ?? "".capitalizingFirstLetter())"

//                        let imageUrl = "\(responseModel.user_image!)"
//                        ImageService.downloadImage(url: URL(string: imageUrl)!) { image in
//                        self.imageView.image = image
//                        }
                    }
                }
                    
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        } .resume()
    }
    
    
    
    func customizingView() {
        
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        let navImage = UIImage(named: "Rectangle 22")
        navigationController?.navigationBar.setBackgroundImage(navImage, for: .default)
        designImage.layer.cornerRadius = 30
        designImage.layer.masksToBounds = true
        designImage.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner,]
        
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = imageView.frame.height / 2
        groupLabel.layer.masksToBounds = true
        groupLabel.layer.borderWidth = 1
        groupLabel.layer.borderColor = UIColor.systemPink.cgColor
        groupLabel.layer.cornerRadius = groupLabel.frame.height / 2
    }
}
