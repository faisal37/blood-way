//
//  AcceptedRequestViewController.swift
//  Blood Way
//
//  Created by S. M. Akib Al Faisal on 30/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class AcceptedRequestViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

var responseData = [AcceptedUserModel]()
    
    @IBOutlet weak var acceptedRequestTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         navigationController?.navigationBar.backItem?.title = "Menu"
               self.title = "Accepted Requests"
        
        acceptedRequestTableView.dataSource = self as? UITableViewDataSource
        acceptedRequestTableView.delegate = self as? UITableViewDelegate
               
        getAcceptedRequestList()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Clear List", style: .plain, target: self, action: #selector(clearAcceptedUserList))
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Menu"
        self.title = "Accepted Requests"
    }
    
   
    
    @objc func clearAcceptedUserList(){
        print("inside clearAcceptedUserList")
    }

    func getAcceptedRequestList(){
               let senderID = getToken(value: "PK")
                
                print(senderID)
                
                //demo data from user profile
                guard let url = URL(string: "\(baseUrl)notification/api/v1/getlist?senderid=\(senderID)&status=true") else { return }
                var request = URLRequest(url: url)
                request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
                request.httpMethod = "GET"
                
                URLSession.shared.dataTask(with: request) { (data, response, error) in
                    
                    if let data = data {
                        do {
                            print("here")
                            let responseModel = try JSONDecoder().decode([AcceptedUserModel].self, from: data)
                            
                            print(responseModel)
                            
                            self.responseData = responseModel
                            
                            DispatchQueue.main.async {
                                
                                self.acceptedRequestTableView.reloadData()
                        
                            }
                        }
                        catch {
                            print(error.localizedDescription)
                            return
                        }
                    }
                } .resume()    }

}

extension AcceptedRequestViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(responseData.count)
        return responseData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AcceptedRequestTableViewCell = acceptedRequestTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AcceptedRequestTableViewCell
        
        cell.nameLabel.text = responseData[indexPath.row].receverName?.capitalizingFirstLetter() ?? ""
        cell.phoneLabel.text = responseData[indexPath.row].receverNumber
        cell.bloodGroupLabel.text = responseData[indexPath.row].receverBloodGroup?.toUpperBG() ?? ""
        cell.locationLabel.text = responseData[indexPath.row].receverLocation?.capitalizingFirstLetter() ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Number of cell Clicked",indexPath.row)
        
        print(responseData[indexPath.row].senderid ?? 0)
        
        let storyboard = UIStoryboard(name: "AcceptedRequest", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AcceptedRequest") as? AcceptedRequest
        vc?.receverID = "\(responseData[indexPath.row].receverid!)"
        vc?.notificationID = "\(responseData[indexPath.row].pk!)"
        navigationController?.pushViewController(vc!, animated: true)
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        return 150
//    }
    
    
    
}
