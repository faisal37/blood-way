//
//  AcceptedRequest.swift
//  Blood Way
//
//  Created by Fahim Rahman on 18/2/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class AcceptedRequest: UIViewController {
    
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

    @IBOutlet weak var profilePicture: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var bloodGroupLabel: UILabel!
    @IBOutlet weak var completedButton: UIButton!
    
    @IBOutlet weak var callNowButton: UIButton!
    
    var receverID = String()
    var notificationID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = "Accepted Requests"
        self.title = "Accepted User"
        
        completedButton.layer.cornerRadius = 25
        callNowButton.layer.cornerRadius = 25
        profilePicture.layer.cornerRadius = profilePicture.frame.height / 2
        
        getProfileData();
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = "Accepted Requests"
        self.title = "Accepted User"
    }
    
    @IBAction func callNowAction(_ sender: UIButton) {
        
        if(phoneLabel.text != nil){
            dialNumber(number: phoneLabel.text!)
        }
    }
    
    @IBAction func completedReceivingBlood(_ sender: Any) {
        
             let alert = UIAlertController(title: "Confirmation", message: "Do you Want to mark this request as completed? This Entry will be deleted", preferredStyle: .alert)
              
              alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

              alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                  self.setRequestConfirmedStatusToProfile(param: false, completion: {
                      success in
                      if(success == true){
                      
                       self.removeReceivedBloodDonationRequest()
                       
                       DispatchQueue.main.async {
                          
                           let alert = UIAlertController(title: "Confirmation", message: "Operation completed successfully", preferredStyle: .alert)
                           alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            
                            self.navigationController?.popToRootViewController(animated: false)
                            self.pushViewControllerExt(storyboardName: "AcceptedRequest", VcIdentifier: "AcceptedRequestViewController")
                           }))
                           self.present(alert, animated: true, completion: nil)
                       }
                      }
                      else{
                          self.alertUser(alertTitle: "Not Successful", alertMessage: "Confirmation Failed", actionTitle: "OK")
                      }
                  } )
              
              }))

              self.present(alert, animated: true)
        
    }
    
    
    func removeReceivedBloodDonationRequest(){
            
              guard let url = URL(string: "\(baseUrl)notification/api/v1/delete/\(notificationID)") else { return }
              var request = URLRequest(url: url)
              request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
              request.httpMethod = "DELETE"
              
              let parameters: [String: Any] = [:]
              
              request.addValue("application/json", forHTTPHeaderField: "Content-Type")

              do {
                  request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
              } catch let error {
                  print(error.localizedDescription)
              }
              
              URLSession.shared.dataTask(with: request) { data, response, error  in
                  
                  do {
                      let responseModel = try JSONDecoder().decode(DeleteResponseConfirm.self, from: data!)

                  }
                  catch {
                      print(error)
                  }
                  
              }.resume()
        }
        
    
    func setRequestConfirmedStatusToProfile(param: Bool, completion: @escaping (_ success: Bool) -> ()){
               var success = true
               guard let url = URL(string: "\(baseUrl)api/v1/account/notificationupdate/\(receverID )") else { return }
               var request = URLRequest(url: url)
               request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
               request.httpMethod = "PUT"
               
         
               let parameters: [String: Any] = [
                   "status" : "\(param)"
               ]
               
               request.setValue("application/json", forHTTPHeaderField: "Content-Type")
               
               print(parameters)
               
               do {
                   
                   request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
               } catch let error {
                   print(error.localizedDescription)
               }
               
               URLSession.shared.dataTask(with: request) { data, response, error  in
                   
                   do {
                       let responseModel = try JSONDecoder().decode(AcceptRequestConfirm.self, from: data!)
                       
                       let confirmResponse = responseModel.response
                       
                        if confirmResponse == "updated"
                           {
                                success = true
                                completion(success)
                            }
                            else {
                                 success = false
                                 completion(success)
                            }
                    }
                   catch {
                       print(error)
                   }
                
               }.resume()
        
    }
    
    func getProfileData() {
        //demo data from user profile
        guard let url = URL(string: "\(baseUrl)api/v1/account/\(receverID)") else { return }
        var request = URLRequest(url: url)
        request.addValue("Token \(token!)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    let responseModel = try JSONDecoder().decode(ProfileModel.self, from: data)
                    DispatchQueue.main.async {
                        self.nameLabel.text = responseModel.username?.capitalizingFirstLetter() ?? ""
                        self.phoneLabel.text = responseModel.mobileNumber
                        self.bloodGroupLabel.text = responseModel.bloodGroup?.toUpperBG() ?? ""
                        self.addressLabel.text = responseModel.area?.capitalizingFirstLetter() ?? ""
                        //                            self.receverPK = object.receverid ?? 0

                        //let imageUrl = "\(responseModel.user_image!)"
                        //ImageService.downloadImage(url: URL(string: imageUrl)!) { image in
                            //self.imageView.image = image
                        //}
                    }
                }
                catch {
                    print(error.localizedDescription)
                    return
                }
            }
        } .resume()
    }
    
   

     func alertUser(alertTitle: String, alertMessage: String, actionTitle: String) {
              let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
              alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { (action) in
               
               self.navigationController?.popToRootViewController(animated: false)
               
               self.pushViewControllerExt(storyboardName: "AcceptedRequest", VcIdentifier: "AcceptedRequestViewController")
              }))
              self.present(alert, animated: true, completion: nil)
          }
    
    
    
}
