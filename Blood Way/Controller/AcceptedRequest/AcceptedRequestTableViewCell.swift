//
//  AcceptedRequestTableViewCell.swift
//  Blood Way
//
//  Created by S. M. Akib Al Faisal on 30/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit

class AcceptedRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var cellContentView: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var bloodGroupLabel: UILabel!

    @IBOutlet weak var profileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellContentView.layer.cornerRadius = 15
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
