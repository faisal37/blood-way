import Foundation

extension UserDefaults{
    
 func setValueForLogin(value:Bool?){
        if value! != false{
            UserDefaults.standard.set(value, forKey: "login")
        }
        else{
            UserDefaults.standard.removeObject(forKey: "login")
    }
        UserDefaults.standard.synchronize()
    }
    
    func getValueofLogin()->Bool?{
        return UserDefaults.standard.value(forKey: "login") as? Bool
    }
    
    func setValueofProfile(username: String?, email: String?, bloodGroup: String? ){
        
        UserDefaults.standard.set(username, forKey: "User")
        UserDefaults.standard.set(email, forKey: "Email")
        UserDefaults.standard.set(bloodGroup, forKey: "BloodGroup")
        
        UserDefaults.standard.synchronize()
    }
    
    func getValueofProfile(value: String?) -> String{
        if(value == "User"){
            let user = UserDefaults.standard.object(forKey: "User") as! String
            return user
        }
        if(value == "Email"){
            let email = UserDefaults.standard.object(forKey: "Email") as! String
            return email
        }
        if(value == "BloodGroup"){
            let bloodgroup = UserDefaults.standard.object(forKey: "BloodGroup") as! String
            return bloodgroup
        }
        
        return ""
    }
    
    func removeValueofProfile(){
        UserDefaults.standard.removeObject(forKey: "User")
        UserDefaults.standard.removeObject(forKey: "Email")
        UserDefaults.standard.removeObject(forKey: "BloodGroup")
    }
    
    
}
