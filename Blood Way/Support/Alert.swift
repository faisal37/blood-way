//
//  Alert.swift
//  Blood Way
//
//  Created by Fahim Rahman on 10/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

//import UIKit
//
//class AlertMaker: UIAlertController {
//
//    func alertUser(alertTitle: String, alertMessage: String, actionTitle: String) {
//        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
//        let action = UIAlertAction(title: actionTitle, style: .default, handler: nil)
//        alert.addAction(action)
//        present(alert, animated: true, completion: nil)
//    }
//
//}
