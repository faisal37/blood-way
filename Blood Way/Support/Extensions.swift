//
//  Extensions.swift
//  Blood Way
//
//  Created by Mac Mini on 16/3/20.
//  Copyright © 2020 Fahim Rahman. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper


extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func toUpperBG() -> String{
        
        if(self.last == "-" || self.last == "+"){
            return self.uppercased()
        }
        else{
            return self.uppercased() + "+"
        }

    }
    
    func toLowerBG() -> String{
        
        if(self.last == "+"){
            return String(self.lowercased().dropLast())
        }
        else{
            return self.lowercased()
        }

    }
}

extension UIViewController{
    
    func dialNumber(number : String) {

     if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
          if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
           } else {
               UIApplication.shared.openURL(url)
           }
       } else {
               let alert = UIAlertController(title: "ERROR", message: "Unable To Call", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                }))
                self.present(alert, animated: true, completion: nil)
       }
    }
    
    func setToken(token: String, pk: String){
            KeychainWrapper.standard.set(token, forKey: "Token")
            KeychainWrapper.standard.set(pk, forKey: "PK")
    }
    
    func getToken(value: String) -> String{
        
        if(value == "Token"){
                  let retrievedString: String? = KeychainWrapper.standard.string(forKey: "Token")
                  return retrievedString ?? ""
              }
        
        if(value == "PK"){
                  let retrievedString: String? = KeychainWrapper.standard.string(forKey: "PK")
                  return retrievedString ?? ""
        }
        
        return ""
              
    }
    
    func removeToken(){
        KeychainWrapper.standard.removeObject(forKey: "Token")
        KeychainWrapper.standard.removeObject(forKey: "PK")
    }
    
    func pushViewControllerExt(storyboardName: String, VcIdentifier: String) {
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: VcIdentifier)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func saveValue(state1: String, state2: String){
        KeychainWrapper.standard.set(state1, forKey: "acceptButton")
        KeychainWrapper.standard.set(state2, forKey: "completeButton")
    }
    
    func getSaveValue(value: String) -> String{
    
    if(value == "acceptButton"){
              let retrievedString: String? = KeychainWrapper.standard.string(forKey: "acceptButton")
              return retrievedString ?? ""
          }
        
    if(value == "completeButton"){
        let retrievedString: String? = KeychainWrapper.standard.string(forKey: "completeButton")
        return retrievedString ?? ""
    }
        
        return ""
        
    }
    
    func removeSaveValue(){
        KeychainWrapper.standard.removeObject(forKey: "acceptButton")
        KeychainWrapper.standard.removeObject(forKey: "completeButton")
        
    }
    
}


extension UIViewController{
    
    func setCustomNavigationBar(backTitle: String, barTitle: String, barTintColor: UIColor){
        navigationController?.navigationBar.backItem?.title = backTitle
        self.title = barTitle
        navigationController?.navigationBar.barTintColor = barTintColor    }
    

    

}

